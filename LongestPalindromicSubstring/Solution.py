'''Longest Palindromic Substring:

A palindrome is a sequence of characters that reads the same backwards and forwards.
Given a string, s, find the longest palindromic substring in s.

This solution sample is written in Python.

Expect output ->

banana -> anana
million -> illi
apple -> pp
roma -> Non-palindromic word.
'''

__author__ = "Langston.Hakurei"
__version__ = "1.0"
__status__ = "Prototype"


class Solution():
    # Find the longest palindromic substring
    def algorithm(self, inputStr):
        reverseStr = "".join(reversed(inputStr))
        longestSubstring = ""
        indexReverse = 0

        for char in reverseStr:
            candidate = ""
            anker = indexReverse
            indexOrigin = 0

            while indexOrigin < len(inputStr) and anker < len(reverseStr):
                # Start to compare with origin string
                if reverseStr[anker] == inputStr[indexOrigin]:
                    candidate = candidate + reverseStr[anker]
                    anker = anker + 1
                    indexOrigin = indexOrigin + 1
                else:
                    indexOrigin = indexOrigin + 1

            if len(candidate) > len(longestSubstring):
                longestSubstring = candidate
                candidate = ""
            indexReverse = indexReverse + 1

        if len(longestSubstring) > 1:
            return longestSubstring
        else:
            return "Non-palindromic word."


inputs = ["banana", "million", "apple", "roma"]
for input in inputs:
    print(input + " -> " + Solution().algorithm(input))
