/*
 * A palindrome is a sequence of characters that reads the same backwards and forwards. 
 * Given a string, s, find the longest palindromic substring in s.
 *
 * This sample solution is written in Java.
 *
 * Expected output:
 *
 * banana -> anana
 * million -> illi
 * apple -> pp
 * roma -> Non-palindromic word.
 *
 * @authur Langston.Hakurei
 * @version 1.0
 * @since 2020-10-24
 */
import java.util.*;
import java.io.*;
import java.lang.*;

class LongestPalindromicSubstring {
    
    String algorithm(String input) {
        StringBuilder inputString = new StringBuilder();
        StringBuilder reversedString = new StringBuilder();
        inputString.append(input);
        reversedString.append(input).reverse();
        
        String answer = "";
        for (int i = 0; i < reversedString.length(); i++) {
            StringBuilder candidate = new StringBuilder();
            int anker = i;
            int index = 0;

            while (anker < reversedString.length() && index < inputString.length()) {
                if (reversedString.charAt(anker) == (inputString.charAt(index))) {
                    candidate.append(reversedString.charAt(anker));
                    anker ++;
                    index ++;
                }
                else {
                    index ++;
                }
            }
            
            if (candidate.length() > answer.length()) {
                answer = candidate.toString();
                candidate.setLength(0);
            }
        }

        if (answer.length() > 1) {
            return answer;
        }
        else {
            return "Non-palindromic word.";
        }
    }
}

public class Solution {
    public static void main(String[] args) {
        String[] samples = {"banana", "million", "apple", "roma"};
        for (String sample : samples) {
            System.out.println(
                sample + 
                " -> " +
                new LongestPalindromicSubstring().algorithm(sample));
        }
    }
}